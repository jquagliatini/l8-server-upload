module.exports = {
  development: {
    client: 'sqlite3',
    connection: {
      filename: './l8-upload.sqlite',
    },
    useNullAsDefault: true,
  },
  test: {
    client: 'sqlite3',
    connection: {
      filename: 'l8-upload.test.sqlite',
    },
    useNullAsDefault: true,
  },
};
