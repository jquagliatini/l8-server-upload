# L8 | Upload Server

This server aims at uploading a file on disk.
At first, it will simply download a file, but the strategy might change
in the future.

## Running

    $ npm install
    $ cp .env.example .env
    $ npm start
    server listens on :3000

## Usage

To use the server you can first launch it.

    $ mkdir -p upload
    $ cp .env.example .env
    $ npm start
      server listens on :3000

To check that it runs well:

    $ curl -i -XOPTIONS http://localhost:3000
    HTTP/1.1 204 No Content
    Access-Control-Allow-Origin: *
    Access-Control-Allow-Methods: POST,PUT
    Access-Control-Allow-Headers: Authorization
    Date: Mon, 20 Aug 2018 13:56:14 GMT
    Connection: keep-alive

Find a file to upload to the server. At the moment, it handles only HTTP sent
files and not multipart (that should come soon).

I did the test with the default video in Windows 7 in `C:\Public\Videos\Sample Videos`
called `Wildlife.wmv`. It's not really large, and should be available in every
Windows OS (at least 7). Though, you can do the same with any file, there is no
limitation at the moment.

To upload the file simply run

    $ export FILE="/my/awesome/file/with.extension"
    $ curl -i --upload-file $FILE http://localhost:3000
    {"written":26246026,"filename":"cx7u2np8wzi"}

If you see a strange name inside the `upload/` folder (found in the `filename`
field in the JSON response) then, _congratulations_!
You just performed the most complicated Copy/Paste **EVER**!

Another thing that you could test is file erasure on upload failure.
To do so, run the same command as above, but hit `Ctrl+C` in the middle
of the upload. It should log something like

    xezzevmid1 removed

Open an issue if you encounter another behaviour (like logging, but no deletion).

## Testing

    $ npm install
    $ npm run test

## Strategies

Uploading a file via HTTP is not a simple thing.
A lot can happen on the wire, especially when you multiply
services and front-facing APIs.
That's why we have multiple available solutions to upload any file.

The current solution to "easily" send a file via HTTP is to use
[multipart][u:multi-spec] encoded data. That's the current encoding
we'll use in that service.

[u:multi-spec]: https://www.w3.org/Protocols/rfc1341/7_2_Multipart.html

### [SC1] Using the raw Node IO model _(current)_

Using [Koa](https://koajs.com) as the main http-handling library was
mainly driven by the existence of the [koa-body](https://npmjs.com/koa-body)
package.
It allows any user to use form, json or multipart encoded body to be sent and
decoded. Under the hood, it uses [formidable](https://github.com/felixge/node-formidable),
which is a fantastic and well-known tool made for this purpose.

The scenario could be described as :

1. Decode Multipart Data
2. Load in memory all the data from the http request
3. Pass the decoded data to the next middlewares
4. Handling the data in a file, or anything else

### [SC2] Letting the HTTP front facing server handle the file

That is the scenario described in this SO post:
https://stackoverflow.com/a/44751210/7493763

### [SC0.1] Using a cloud-provider file storage

- [AWS S3](https://aws.amazon.com/fr/s3/)
- [Google Cloud Storage](https://cloud.google.com/storage)
- [OVH Object Storage](https://www.ovh.com/fr/public-cloud/storage/object-storage/)

are some of the possibilities (compatible with both [SC1] and [SC2] scenarios).

### [SC0.2] Using a third-party file service

- [LibCast](https://www.libcast.com/)
- [Novastream](https://www.novastream.fr/solutions/)

are some of the possibilities (compatible with both [SC1] and [SC2] scenarios).

See [DEV-29][jira:DEV-29].

[jira:DEV-29]: https://jira.maximeboisson.fr/browse/DEV-29
