/* eslint-env jest */

const corsOrigin = require('../src/middlewares/corsOrigin.js');

test('corsOrigin', (done) => {
  let state = {};
  const ctx = {
    set(headers) {
      state.headers = headers;
    }
  }

  corsOrigin(
    ctx,
    () => {
      expect(state.headers).toHaveProperty('Access-Control-Allow-Origin', '*');
      done();
    },
  );
});
