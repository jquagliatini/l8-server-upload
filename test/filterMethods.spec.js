/* eslint-env jest */

const filterMethods = require('../src/middlewares/filterMethods.js');

test('[GET] filterMethods', () => {
  const ctx = {
    request: {
      method: 'GET',
    },
    throw(status, message) {
      const e = new Error(message);
      e.code = status;
      throw e;
    }
  };

  try {
    filterMethods(ctx)
  } catch (e) {
    expect(e.message).toBe('Wrong Method');
    expect(e.code).toBe(405);
  }
});

['POST', 'PUT'].forEach((method) => {
  test(`[${method}] filterMethods`, (done) => {
    filterMethods({
      request: { method },
    }, done);
  });
});
