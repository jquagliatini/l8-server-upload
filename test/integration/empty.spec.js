/* eslint-env jest */
const supertest = require('supertest');

const app = require('../../src/server.js');

const server = app.listen();
const request = supertest.agent(server);

afterAll(async () => server.close());

describe('[integration] Empty Responses', () => {
  test('[GET] /', () => request.get('/').expect(405));

  test('[OPTIONS] / without authorization', () =>
    request
      .options('/')
      .expect(403)
      .expect('Access-Control-Allow-Origin', '*'));

  test('[OPTIONS] /', () =>
    request
      .options('/')
      .expect(204)
      .set('Authorization', 'Bearer TOKEN')
      .expect('Access-Control-Allow-Origin', '*')
      .expect('Access-Control-Allow-Methods', 'POST,PUT')
      .expect('Access-Control-Allow-Headers', 'Authorization'));

  test('[GET] /_health', () =>
    request
      .get('/_health')
      .expect(204)
      .expect('Access-Control-Allow-Origin', '*'));
});
