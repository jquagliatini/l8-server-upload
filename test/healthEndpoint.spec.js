/* eslint-env jest */

const healthEndpoint = require('../src/middlewares/healthEndpoint.js');

describe('healthEndpoint', () => {
  test('[GET] /_health', () => {
    let ctx = {
      url: '/_health',
    };

    return healthEndpoint(ctx).then(() => {
      expect(ctx.status).toBe(204);
    });
  });

  test('[GET] not health', () => new Promise((resolve) =>
    healthEndpoint({
      url: '/anotherThing',
    }, resolve)));

});
