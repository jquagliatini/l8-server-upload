/* eslint-env jest */
const checkEnv = require('../src/utils/checkEnv.js');

/* Should evolve together with the function */
const processEnv = {
  NODE_ENV: 'test',
  PORT: 3000,
  AUTH_SVC_ADDR: '127.0.0.1',
  AUTH_SVC_PORT: 3000,
};

afterAll(() => {
  Object.keys(processEnv).forEach((key) => {
    delete process.env[key];
  });
});

describe('checkEnv', () => {
  const t = () => checkEnv();
  it('should throw', () => {
    expect(t).toThrow('PORT should be defined as envvar!');
  });

  it('should not throw if every expected envvar is defined', () => {
    process.env = processEnv;
    expect(t).not.toThrow();
  });
});
