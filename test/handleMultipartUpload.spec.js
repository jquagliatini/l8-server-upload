/* eslint-env jest */

const handleMultipartUpload =
  require('../src/middlewares/handleMultipartUpload.js');

const throwHttpError = (status, mess) => {
  throw Object.assign(
    new Error(mess),
    { code: status },
  );
};

describe('handleMultipartUpload', () => {
  test('no file throws 400', () => {
    try {
      handleMultipartUpload(
        {
          request: { method: 'GET' },
          throw: throwHttpError,
        },
      )
    } catch (e) {
      expect(e.code).toBe(400);
      expect(e.message).toBe('Expected multipart!');
    }
  });

  test('unique file per field', () => {
    let ctx = {
      request: {
        method: 'PUT',
        files: {
          firstFile: {
            size: 32,
            path: 'filename',
          },
        },
      },
    };

    handleMultipartUpload(ctx);

    expect(ctx.request.files).toStrictEqual([
      { size: 32, path: 'filename' },
    ]);
  });

  test('multiple files on multiple fields', () => {
    let ctx = {
      request: {
        method: 'PUT',
        files: {
          firstFile: [
            { size: 32, path: 'filename1' },
            { size: 32, path: 'filename2' },
          ],
          secondFile: [
            { size: 32, path: 'filename3' },
            { size: 32, path: 'filename4' },
          ],
        },
      }
    };

    handleMultipartUpload(ctx);

    expect(ctx.request.files).toStrictEqual([
      { size: 32, path: 'filename1' },
      { size: 32, path: 'filename2' },
      { size: 32, path: 'filename3' },
      { size: 32, path: 'filename4' },
    ]);
  });
});
