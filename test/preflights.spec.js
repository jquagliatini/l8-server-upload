/* eslint-env jest */

const preflights = require('../src/middlewares/preflights.js');

describe('preflights', () => {
  test('[GET]', () =>
    new Promise((resolve) => {
      preflights({ method: 'GET' }, resolve);
    }));

  test('[OPTIONS]', () => {
    const headers = {};

    const ctx = {
      method: 'OPTIONS',
      req: {
        headers: {
          authorization: 'Bearer TOKEN',
          expect: '100-continue',
        },
      },
      res: {
        writeContinue() {},
      },
      request: {
        get() {
          return 'Authorization,X-Token';
        },
      },
      set(header, value) {
        headers[header] = value;
      },
    };

    return preflights(ctx).then(() => {
      expect(headers).toStrictEqual({
        'Access-Control-Allow-Methods': 'POST,PUT',
        'Access-Control-Allow-Headers': 'Authorization,X-Token',
      });
    });
  });
});
