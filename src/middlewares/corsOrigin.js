module.exports = function corsOrigin(ctx, next) {
  ctx.set({ 'Access-Control-Allow-Origin': '*' });
  return next();
};
