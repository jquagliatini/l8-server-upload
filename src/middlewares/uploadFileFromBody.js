const fs = require('fs');
const log = require('signale');
const { sep: PATH_SEP } = require('path');

module.exports = (ctx, next) => {
  if (ctx.is('multipart/form-data')) {
    return next();
  }

  const fileDest = `upload${PATH_SEP}${ctx.state.filename()}`;

  log.info(`will write ${fileDest}`);

  // FIXME: check upload folder existance before server start!
  const out = fs.createWriteStream(fileDest);
  return new Promise((resolve, reject) => {
    ctx.state.errorHandler = (e, innerCtx) => {
      log.error(e);

      fs.unlink(fileDest, (fsErr) => {
        if (fsErr) {
          if (innerCtx) {
            delete innerCtx.state.errorHandler; // eslint-disable-line
          }
          innerCtx.app.emit('error', fsErr);
        } else {
          log.info(`${fileDest} removed`);
        }
      });
    };

    ctx.req
      .on('error', reject)
      .on('end', () => {
        out.end();
        out.close();
        ctx.request.files = [{ size: out.bytesWritten, path: fileDest }];
        resolve();
      })
      .pipe(out);
  });
};
