const log = require('signale');
const { promisify } = require('util');
const { rename } = require('fs');

const mv = promisify(rename);

/**
 * A file description object
 * @typedef {{ path: String, size: Number }} File
 */

/**
 * This function transforms a File into our
 * Data Transfert Object, which contains `writtent` and `filename` properties.
 *
 * @private
 * @param {File} file the file description object.
 * @return {{ filename: String, written: Number }} the representation in our
 * REST environment of the upload result.
 */
const fileDTO = (file) => ({
  filename: file.path.replace(new RegExp(`^upload\\${PATH_SEP}`), ''),
  written: file.size,
});

/**
 * rename a file from `file.path` to `newName`.
 *
 * @param {() => String} name the filename fabric.
 * @return {(File) => Promise<File>} a function taking a file object.
 * */
const renameFile = (name) =>
  /**
   * @param {File} file a file description
   * @return {Promise<File>} when the rename operation is done with the new path
   */
  (file) => {
    const newPath = `upload${PATH_SEP}${name()}`;
    log.info(`will write ${newPath}`);
    return mv(file.path, newPath).then(() => ({
      path: newPath,
      size: file.size,
    }));
  };

/**
 * - checks the authentication
 * - define a unique random name for the uploaded file
 * - await for the upload to complete,
 * - and define a response object accordingly.
 *
 * The idea is to have a single outcome after the upload:
 * - renaming files (to remove the 'upload_' prefix)
 * - standardizing output
 * - Writing in Db (TODO).
 */
module.exports = async (ctx, next) => {
  /** @see checkAuth */
  if (!ctx.req.authChecked) {
    ctx.throw(403);
  }

  // The new filename is provided to the next middlewares
  ctx.state.filename = () => randomBytes(5).toString('hex');

  // next should define a `ctx.request.files` property.
  await next();

  if (!ctx.request.files) {
    ctx.throw(400);
  }

  // When the Content-Type is 'multipart' we will handle it with a
  // special middleware `koaBody`. In that case, the files will
  // need to be renamed.
  const files = await (ctx.is('multipart/form-data')
    ? Promise.all(ctx.request.files.map(renameFile(ctx.state.filename)))
    : Promise.resolve(ctx.request.files));

  ctx.status = 201;
  ctx.body = files.map(fileDTO);
};
