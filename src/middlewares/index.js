const corsOrigin = require('./corsOrigin.js');
const filterMethods = require('./filterMethods.js');
const getUploadedFiles = require('./getUploadedFiles.js');
const handleMultipartUpload = require('./handleMultipartUpload');
const healthEndpoint = require('./healthEndpoint.js');
const preflights = require('./preflights.js');
const uploadFileFromBody = require('./uploadFileFromBody.js');

module.exports = {
  corsOrigin,
  filterMethods,
  getUploadedFiles,
  handleMultipartUpload,
  healthEndpoint,
  preflights,
  uploadFileFromBody,
};
