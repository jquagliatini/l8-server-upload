const checkAuth = require('../utils/checkAuth.js');

/* This middleware handles preflight requests */
module.exports = async (ctx, next) => {
  if (ctx.method === 'OPTIONS') {
    await new Promise((resolve) => {
      // TODO: is it mandatory to check for auth for preflight ?
      checkAuth(resolve)(ctx.req, ctx.res);
    }).then(() => {
      ctx.set('Access-Control-Allow-Methods', 'POST,PUT');

      if (ctx.request.get('Access-Control-Request-Headers')) {
        ctx.set(
          'Access-Control-Allow-Headers',
          ctx.request.get('Access-Control-Request-Headers'),
        );
      }

      ctx.status = 204;
    });
  } else {
    await next();
  }
};
