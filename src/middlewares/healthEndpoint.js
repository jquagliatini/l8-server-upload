/** our standard /_health endpoint to check that the service is still up */
module.exports = async (ctx, next) => {
  if (ctx.url !== '/_health') {
    await next();
  } else {
    ctx.status = 204;
  }
};
