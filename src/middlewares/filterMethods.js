/** Only `PUT` and `POST` queries should be accepted */
module.exports = (ctx, next) => {
  if (!['PUT', 'POST'].includes(ctx.request.method)) {
    ctx.throw(405, 'Wrong Method');
  }

  return next();
}
