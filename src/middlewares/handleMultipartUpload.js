module.exports = (ctx) => {
  if (!ctx.request.files) {
    ctx.throw(400, 'Expected multipart!');
  }

  ctx.request.files = Object.values(ctx.request.files).reduce(
    (ar, file) => ar.concat(file),
    [],
  );
};
