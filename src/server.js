const Koa = require('koa');
const koaBody = require('koa-body');
const { createServer } = require('http');

const checkAuth = require('./utils/checkAuth.js');

const {
  corsOrigin,
  filterMethods,
  getUploadedFiles,
  handleMultipartUpload,
  healthEndpoint,
  preflights,
  uploadFileFromBody,
} = require('./middlewares/index.js');

const app = new Koa();

app.use(corsOrigin);
app.use(healthEndpoint);
app.use(preflights);
app.use(filterMethods);
app.use(getUploadedFiles);
app.use(uploadFileFromBody);

app.use(
  koaBody({
    multipart: true,
    formidable: {
      uploadDir: 'upload',
    },
  }),
);

app.use(handleMultipartUpload);

app.on('error', (e, ctx) => {
  if (ctx && ctx.state.errorHandler) {
    ctx.state.errorHandler(e, ctx);
  } else {
    app.onerror(e);
  }
});

const callback = app.callback();
const httpServer = createServer(callback);

httpServer.on('checkContinue', checkAuth(callback));

module.exports = httpServer;
