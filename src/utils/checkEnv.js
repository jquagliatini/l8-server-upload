const hasProperty = (obj) => (property) =>
  Object.prototype.hasOwnProperty.call(obj, property);

const toCheck = ['NODE_ENV', 'PORT', 'AUTH_SVC_ADDR', 'AUTH_SVC_PORT'];

module.exports = function checkEnv() {
  const envExists = hasProperty(process.env);
  const unexistingEnvvar = toCheck.find((k) => !envExists(k));
  if (unexistingEnvvar) {
    throw new Error(`${unexistingEnvvar} should be defined as envvar!`);
  }
};
