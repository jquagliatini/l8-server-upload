const http = require('http');

const writeContinue = (req, res, callback) => {
  if (
    req.headers &&
    req.headers.expect &&
    req.headers.expect === '100-continue'
  ) {
    res.writeContinue();
  }
  callback(Object.assign(req, { authChecked: true }, res));
};

/**
 * @param {IncomingMessage} req
 * @param {ServerResponse} res
 */
module.exports = function checkAuth(callback) {
  return (req, res) => {
    if (
      !req.headers.authorization ||
      !req.headers.authorization.startsWith('Bearer')
    ) {
      res.writeHead(403);
      res.end();
      return;
    }

    if (process.env.NODE_ENV !== 'test') {
      http
        .get(
          {
            protocol: 'http:',
            host: process.env.AUTH_SVC_ADDR,
            port: process.env.AUTH_SVC_PORT,
            path: '/u/_check',
            headers: {
              Authorization: `${req.headers.authorization}`,
            },
            timeout: 1000,
          },
          (authRes) => {
            if (authRes.statusCode > 299) {
              res.writeHead(403);
              res.end();
              return;
            }

            writeContinue(req, res, callback);
          },
        )
        .on('error', (err) => {
          res.writeHead(403);
          res.end(
            process.env.NODE_ENV !== 'production'
              ? err.message || 'Auth server unreachable'
              : '',
          );
        });
    } else {
      writeContinue(req, res, callback);
    }
  };
};
