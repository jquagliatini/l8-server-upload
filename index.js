// TODO: Switch to **HTTPS**
const { config } = require('dotenv');

config();

const server = require('./src/server.js');
const checkEnv = require('./src/utils/checkEnv.js');

checkEnv();

server.listen(process.env.PORT);

console.log(`server listens on :${process.env.PORT}`);
